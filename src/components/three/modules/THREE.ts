declare namespace THREE {
  export class Vector3 {
    x: number;
    y: number;
    z: number;
    constructor(x?: number, y?: number, z?: number);
    // 其他属性和方法...
  }
}