
declare module 'three'
declare module 'three/examples/jsm/postprocessing/EffectComposer'
declare module 'three/examples/jsm/renderers/CSS2DRenderer'
declare module 'three/examples/jsm/renderers/CSS3DRenderer'
declare module 'three/examples/jsm/controls/OrbitControls.js'
declare module 'three/examples/jsm/libs/stats.module.js'
declare module 'three/examples/jsm/objects/Lensflare.js'
declare module 'three/examples/jsm/loaders/GLTFLoader.js'
declare module 'three/examples/jsm/loaders/DRACOLoader.js'
declare module 'three/examples/jsm/loaders/FBXLoader'
declare module 'three/examples/jsm/utils/SkeletonUtils.js'
declare module 'three/examples/jsm/libs/tween.module.min.js'
declare module 'three/examples/jsm/interactive/HTMLMesh.js'
declare module 'three/examples/jsm/interactive/InteractiveGroup.js'
declare module 'three/examples/jsm/postprocessing/ShaderPass'


